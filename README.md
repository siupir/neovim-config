# Neovim configuration

Mostly based on [ChrisAtMachine](https://www.youtube.com/channel/UCS97tchJDq17Qms3cux8wcA) video series [Neovim IDE from Scratch](https://www.youtube.com/watch?v=ctH-a-1eUME)

Good resource for plugins and other good stuff is [Awesome Neovim](https://github.com/rockerBOO/awesome-neovim)

## Troubleshooting

* If using Alacritty as terminal emulator on MacOS `ALT` key works differently and keybindings with this midifier is not working. To fix this [custom keybindings](https://github.com/alacritty/alacritty/issues/62#issuecomment-347552058) should be added to Alacritty `key_bindings:` section.

## Things to consider

* [Trouble](https://github.com/folke/trouble.nvim)
* [Neorg](https://github.com/nvim-neorg/neorg)

## TODO

* Orgmode ?

