-- default options for keymap
local opts = { noremap = true, silent = true } -- TODO: silent set to true

-- terminal keymap options
local term_opts = { silent = true } -- TODO: silent set to true

-- shorten name
local km = vim.api.nvim_set_keymap

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- set leader to space
km("", "<,>", "<Nop>", opts)
vim.g.mapleader = ","
vim.g.maplocalleader = ","

-- > NORMAL MODE --
-- Window navigation --
km("n", "<C-h>", "<C-w>h", opts)
km("n", "<C-j>", "<C-w>j", opts)
km("n", "<C-k>", "<C-w>k", opts)
km("n", "<C-l>", "<C-w>l", opts)

-- navigate buffers
km("n", "<S-l>", ":bnext<CR>", opts)
km("n", "<S-h>", ":bprevious<CR>", opts)

-- open file explorer
km("n", "<leader>e", ":NvimTreeToggle<CR>", opts)

-- faster navigation to beginning/end of the line
km("n", "<leader>a", "^", opts)
km("n", "<leader>d", "$", opts)
-- < NORMAL MODE --

-- > VISUAL MODE --
-- stay in indent mode
km("v", "<", "<gv", opts)
km("v", ">", ">gv", opts)

-- keep previous register when replacing with visual mode
km("v", "p", '"_dP', opts)
-- < VISUAL MODE --

-- > VISUAL BLOCK MODE --
-- Move text up and down
km("x", "J", ":move '>+1<CR>gv-gv", opts)
km("x", "K", ":move '<-2<CR>gv-gv", opts)
km("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
km("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)
-- < VISUAL BLOCK MODE --

-- > TELESCOPE --
km(
  "n",
  "<leader>f",
  "<cmd>lua require'telescope.builtin'.find_files(require('telescope.themes').get_dropdown({ previewer = false }))<cr>",
  opts
)
km(
  "n",
  "<leader>b",
  "<cmd>lua require'telescope.builtin'.buffers(require('telescope.themes').get_dropdown({ previewer = false }))<cr>",
  opts
)
km(
  "n",
  "<leader>r",
  "<cmd>lua require'telescope.builtin'.grep_string(require('telescope.themes'))<cr>",
  opts
)
km("n", "<c-t>", "<cmd>Telescope live_grep<cr>", opts)
-- < TELESCOPE --

-- > TABS --
-- km("n", "[b", ":BufferLineCycleNext<CR>", opts)
-- km("n", "b]", ":BufferLineCyclePrev<CR>", opts)
-- < TABS --

-- > FLOATING TERMINAL --
km("n", "<A-i>", '<cmd> lua require("FTerm").toggle()<cr>', opts)
km("t", "<A-i>", '<C-\\><C-n><cmd> lua require("FTerm").toggle()<cr>', opts)
-- < FLOATING TERMINAL --

-- > LSP --
km("n", "<leader>w", "<cmd>lua vim.lsp.buf.formatting_sync()<cr>", opts)
-- < LSP --

--> Persistence --
km("n", "<leader>qs", '<cmd> lua require("persistence").load()<cr>', opts)
km("n", "<leader>ql", '<cmd> lua require("persistence").load({ last = true })<cr>', opts)
--< Persistence --
