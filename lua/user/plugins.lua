local fn = vim.fn

-- Automatically install packer

local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system({
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  })
  print("Installing packer close and reopen Neovim...")
  vim.cmd([[packadd packer.nvim]])
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

-- Have packer use a popup window
packer.init({
  display = {
    open_fn = function()
      return require("packer.util").float({ border = "rounded" })
    end,
  },
})

-- Install your plugins here
return packer.startup(function(use)
  -- My plugins here
  -- CORE plugins
  use("wbthomason/packer.nvim") -- Have packer manage itself
  use("nvim-lua/popup.nvim") -- An implementation of the Popup API from vim in Neovim
  use("nvim-lua/plenary.nvim") -- Useful lua functions used ny lots of plugins
  use("lewis6991/impatient.nvim") -- improve startup time
  use("kyazdani42/nvim-web-devicons") -- common for multiple plugins
  use({"folke/persistence.nvim", event = "BufReadPre", module = "persistence"}) -- nvim session management

  -- improve ui and comfort
  use("windwp/nvim-autopairs") -- autopairs for brackets, html tags etc.
  use("numToStr/Comment.nvim") -- enchanted commenting (linewise and blockwise)
  use("JoosepAlviste/nvim-ts-context-commentstring") -- comments based on the context code
  use("numToStr/FTerm.nvim") -- floating terminal (scratchpad)
  use("kyazdani42/nvim-tree.lua") -- sidebar project tree
  use("folke/which-key.nvim") -- show keybindings

  -- colorschemes
  use("lunarvim/colorschemes") -- some color schemes from lunar vim
  use("folke/tokyonight.nvim") -- some nice color schemes
  use("rose-pine/neovim") -- nice violet theme

  -- Status line
  use("nvim-lualine/lualine.nvim")

  -- Tabline
  use({ "akinsho/bufferline.nvim", branch="main", requires = "kyazdani42/nvim-web-devicons" })
  use("moll/vim-bbye")

  -- Code completation plugins
  use("hrsh7th/nvim-cmp") -- The completion plugin
  use("hrsh7th/cmp-buffer") -- buffer completions
  use("hrsh7th/cmp-path") -- path completions
  use("hrsh7th/cmp-cmdline") -- command line completions
  use("saadparwaiz1/cmp_luasnip") -- snippet comletions
  use("hrsh7th/cmp-nvim-lsp") -- lsp completions

  -- snippets
  use("L3MON4D3/LuaSnip") --snippet engine
  use("rafamadriz/friendly-snippets") -- many snippets one place

  -- LSP
  use("neovim/nvim-lspconfig") -- enable LSP
  use("williamboman/nvim-lsp-installer") -- simple LSP server installer
  use("jose-elias-alvarez/null-ls.nvim") -- LSP diagnostics, code actions

  -- Telescope
  use("nvim-telescope/telescope.nvim")

  -- TreeSitter
  use({
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
  })
  use("p00f/nvim-ts-rainbow")

  -- GIT
  use("lewis6991/gitsigns.nvim")

  -- Neorg
  use("nvim-neorg/neorg")

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
