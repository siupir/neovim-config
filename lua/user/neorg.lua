local status_ok, neorg = pcall(require, "neorg")
if not status_ok then
  return
end

neorg.setup({
  -- Tell Neorg what modules to load
  load = {
    ["core.defaults"] = {}, -- Load all the default modules
    ["core.norg.concealer"] = {}, -- Allows for use of icons
    ["core.norg.dirman"] = { -- Manage your directories with Neorg
      config = {
        workspaces = {
          personal = "~/Code/neorg/personal",
          work = "~/Code/neorg/work",
          gtd = "~/Code/neorg/gtd",
        },
        autochdir = true,
        index = "index.norg",
        last_workspace = vim.fn.stdpath("cache") .. "/neorg_last_workspace.txt",
      },
    },
    ["core.norg.completion"] = {
      config = {
        engine = "nvim-cmp",
      },
    },
    ["core.keybinds"] = { -- Configure core.keybinds
      config = {
        default_keybinds = true,
        neorg_leader = "<Leader>o", -- this is default leader
      },
    },
    ["core.gtd.base"] = {
      config = {
        workspace = "gtd",
        default_lists = "inbox.norg",
      },
    },
  },
})
