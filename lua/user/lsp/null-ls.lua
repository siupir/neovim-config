local status_ok, null_ls = pcall(require, "null-ls")
if not status_ok then
  vim.notify("null-ls was not loaded")
  return
end

local formatting = null_ls.builtins.formatting
local diagnostics = null_ls.builtins.diagnostics

null_ls.setup({
  sources = {
    formatting.stylua.with({
      extra_args = { "--indent-type=Spaces", "--indent-width=2" },
    }),
    diagnostics.eslint_d,
    formatting.prettier.with({
      extra_args = { "--indent-type=Spaces", "--indent-width=2" },
      filetypes = { "html", "json", "yaml", "markdown", "vue", "tsx", "jsx", "js", "ts", "css" },
    }),
    formatting.phpcsfixer.with({}),
  },
})
