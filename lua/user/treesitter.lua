local configs = require("nvim-treesitter.configs")
configs.setup({
  ensure_installed = "all",
  sync_install = false,
  ignore_install = { "" }, -- List of parsers to ignore installing
  autopairs = {
    enable = true,
  },
  highlight = {
    enable = true, -- false will disable the whole extension
    disable = { "" }, -- list of language that will be disabled
    additional_vim_regex_highlighting = true,
  },
  indent = { enable = true, disable = { "yaml" } },
  rainbow = {
    enable = true,
    -- disable = { }, -- list of languages to disable rainbow brackets
    extended_mode = true, -- enable for non-bracket like html tags, boolean or table: lang -> boolean
    max_file_lines = nil, -- do not eanble for files more than n lines (int)
    --  colors = { }, -- hex colors to use
    -- termcolors = { } -- table of color name strings
  },
  context_commentstring = {
    enable = true,
    enable_autocmd = false,
  },
})

local parser_configs = require("nvim-treesitter.parsers").get_parser_configs()
parser_configs.twig = {
  install_info = {
    url = "~/Code/tree-sitter-twig",
    files = { "src/parser.c", "src/scanner.cc" },
    branch = "main",
  },
  filetype = "twig",
}
--
-- parser_configs.norg = {
--     install_info = {
--         url = "https://github.com/nvim-neorg/tree-sitter-norg",
--         files = { "src/parser.c", "src/scanner.cc" },
--         branch = "main"
--     },
-- }
--
-- parser_configs.norg_meta = {
--     install_info = {
--         url = "https://github.com/nvim-neorg/tree-sitter-norg-meta",
--         files = { "src/parser.c" },
--         branch = "main"
--     },
-- }
--
-- parser_configs.norg_table = {
--     install_info = {
--         url = "https://github.com/nvim-neorg/tree-sitter-norg-table",
--         files = { "src/parser.c" },
--         branch = "main"
--     },
-- }
